This book contains writeups about the internal workings of The Legend of Zelda: The Minish Cap.
It aims to cover every little detail one might want to know.
If anything isn't covered, it should be added.

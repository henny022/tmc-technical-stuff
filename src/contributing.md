# How to contribute to this book
There are many ways to contribute to the completeness and correctness of this book.

You can contribute even without any deep knowledge about anything.
All you need to do, is to read and provide feedback.
If anything seems unclear or some information you are looking for is missing, tell me and I will do my best to improve the book.
To contact me for these purposes, just ping me (`Henny022`) on the [TMC Speedrunning](https://discord.gg/vtRnrKu) or [Minish Maker](https://discord.gg/ndFuWbV) discord server.
Or (if you want to save me some work) open an issue on the GitLab repository [issue tracker](https://gitlab.com/henny022/tmc-technical-stuff/-/issues).

## Contributing text
If you want to contribute by writing some stuff yourself, this book is open source and accepting merge requests.
There should be a <i class="fa fa-edit"></i> button somewhere in the top right of the screen.
This directly links to editing the current page on gitlab.
This is great for simple changes, as it handles all the git stuff for you.
For more complex additions, like adding whole chapters, you probably want to fork the repository instead.
I'm not going to explain how git works (that's just not the appropriate place).
But I'd be happy to help you out with any problems over on discord.

## Style guide
A few simple style rules for writing text for this book:
- One sentence per line.
  Makes it easier to keep track of text structure and sentence length in the source code and the Markdown renderer takes care of formatting.
- use code blocks whenever appropriate and annotate them with the correct language to allow for syntax highlighting in the rendered book

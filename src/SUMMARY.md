# Summary

[Introduction](introduction.md)
# Random stuff
- [random number generator (PRNG)](prng.md)
- [Droptables](droptables.md)
# Meta
- [Contributing](contributing.md)

TMC droptables have 16 entries containing weights corresponding to different items, though the last 3 slots are unused.
Slots in order correspond to:
- nothing
- green rupee
- blue rupee
- red rupee
- heart
- fairy
- bomb refill
- arrow refill
- mysterious shell
- red kinstone
- blue kinstone
- green kinstone
- beetle (spawns the beetle enemy)
- nothing (unused)
- nothing (unused)
- nothing (unused)

The kinstone drops do not correspond to an actual item, but to a pseudo item that is then replaced by the actual kinstone item.
The shape is determined by another random call.
The beetle drop is another pseudo item, if it is selected as a drop instead of dropping an item, the beetle enemy is spawned instead.

# generating droptables
The game creates droptables dynamically from a set of different factors.
There are 4 scenarios when generating droptables:
- enemy drops (mostly through DeathFx)
- object drops (mostly through SpecialFx)
- area drops
- unknown mole mitts (broken on EU)

Depending on the scenario one base droptable is created.
Then modifiers based on the players stats and inventory are applied.

## creating enemy droptables
Enemies drop items through the DeathFx entity.
One of a few droptables is chosen based on the enemy id.
DeathFx droptables are independent of the area droptable.

## creating object droptables
Objects drop items through the SpecialFx entity.
One of a few droptables is chosen based on the SpecialFx type.
SpecialFx drop do depend on the area droptable.

## the area droptable
The area droptable is dependent on the location index.
The location index is loaded dependent on a rooms area, but can be overwritten by tile entity 12.
Based on this location index an area droptable is loaded, but it can be overwritten by tile entity 11.

## droptable modifiers
### picolyte
Active picolyte does apply modifiers.
**TODO**

### stats and inventory
Player stats and inventory affect droptables.
- low health (<= 1 heart): +5 heart weight
- no bombs: +3 bomb refill weight
- no arrows: +3 arrow refill weight
- low rupees (<= 10 rupees): +1 blue rupee weight

### progression
There are two progression based droptable modifiers, one for after completing all fusions and one for collecting all figurines.

## final droptable
The final droptable is built by summing up the item weights from each modifier table.
Then clamp negative weights to 0.
The dropped item is determined by a random number between 0 and the sum of all weights in the table.

# kinstone drops
If one of the three kinstone pseudo items is chosen as a random drop, the shape of the kinstone will be determined by another random call.
This is done with a lookup table that has an even distribution of each shape.
To get this even distribution, the table for green and red kinstones has an entry for no drop.
This means even if a green or red kinstone is chosen to drop from the droptable, there is a 1/64 chance that no kinstone will drop after all.
Blue kinstones do not suffer from this problem, because since there are only two blue kinstone shapes, no such filler is needed to get an even distribution of shapes.

# item drop overrides
Some items cannot drop under certain conditions.
These rules ae applied after determining a random drop, so they do not affect the creation of the droptable.
These rules are:
- beetle does not drop if you do not have the smith sword
- bomb refills do not drop if you do not have the bomb bag
- arrow refill do not drop if you do not have the bow
- mysterious shells do not drop if you do not have the earth element
- kinstones do not drop if you do not have the kinstone bag

# what is a pseudo random number generator
A pseudo random number generator generates seemingly random numbers.
It does so by performing different operations on an internal state, producing a seeming random numbers as a result.
Because of this, a given PRNG will always generate the exact same sequence of numbers from a given state.
This is why it has pseudo in the name and makes a PRNG result predictable.

# what is a PRNG seed
A seed is a value that is used to set the PRNG internal state to a fixed, known state.
It's main use it to initialize the PRNG.
The state derived from the seed can be identical to the seed value, but does not have to.
Sometimes the term seed is also used to describe the PRNG state, using terms like `initial seed` for the starting seed value and `current seed` for the PRNG state.

# the TMC PRNG
The PRNG used by TMC looks like follows:
```
Random:
	ldr r2, .L1 @ =gRand
	ldr r0, [r2]
	lsls r1, r0, #1
	adds r0, r0, r1
	movs r1, #0xd
	rors r0, r1
	str r0, [r2]
	lsrs r0, r0, #1
	bx lr
.L1:
    .4byte gRand
```
Translated to C++ if looks like this:
```c++
extern u32 gRand;
u32 Random() {
    gRand = std::rotr(gRand * 3, 13);
    return gRand >> 1;
}
```
It produces 31 bit pseudo random numbers from a 32 bit PRNG state.
It is not really a good PRNG, but it's fast and does it's job.
The PRNG state is seeded to `0x1234567` when the game is launched.
This seed gives us a period of `822119800`, after that the sequence of PRNG states repeats from the beginning.
